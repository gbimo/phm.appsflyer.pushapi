package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func AppsflyerHandler(c echo.Context) error {

	result := echo.Map{}

	if err := c.Bind(&result); err != nil {
		return err
	}
	return  c.JSON(http.StatusCreated, result)
}
