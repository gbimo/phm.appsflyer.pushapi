package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	"net/http"
)

type HealthState struct {
	State 		int
	ErrorMessages []string
}

func HealthHandler(c echo.Context) error {

	outputState := &HealthState{}
	checkweb(outputState)
	checkDB(outputState)

	if len(outputState.ErrorMessages) > 0 {
		outputState.State = http.StatusServiceUnavailable
	} else {outputState.State = http.StatusOK}

	c.Response().Header().Set("Content-Type", "application/json")
	c.Response().WriteHeader(outputState.State)
	return  json.NewEncoder(c.Response()).Encode(outputState)

}


func checkweb(s *HealthState) {

	resp, err := http.Get("https://gustibimo.com")
	if err != nil || resp.StatusCode < 200 || resp.StatusCode > 299 {
		s.ErrorMessages = append(s.ErrorMessages, fmt.Sprintf("WebUI: %s", err.Error()))
	}
}

func checkDB(s *HealthState) {
	db, err := sql.Open("mysql", "user:pass@tcp(127.0.0.1:3306)/dbname")
	if err != nil {
		s.ErrorMessages = append(s.ErrorMessages, fmt.Sprintf("MYSQL: %s", err.Error()))
	} else {
		defer db.Close()
	}
	err = db.Ping()
	if err != nil {
		s.ErrorMessages = append(s.ErrorMessages, fmt.Sprintf("MYSQL: %s", err.Error()))
	}
}
