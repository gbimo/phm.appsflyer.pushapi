package main

import (
	handler "appsflyer-push/handler"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)


func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/health", handler.HealthHandler)
	e.POST("/appsflyer", handler.AppsflyerHandler)

	// Start server
	e.Logger.Fatal(e.Start(":8050"))
}


