package entities


type Appsflyer struct {
	AttributedTouchType string `json:"attributed_touch_type"`
	InstallTime 		string `json:"install_time"`
	EventTime 			string	`json:"event_time"`
	EventName 			string	`json:"event_name"`
	EventValue 			string	`json:"event_value"`
	EventRevenue 		string	`json:"event_revenue"`
	EventRevenueCurrency	string `json:"event_revenue_currency"`
	EventRevenueIDR 	string `json:"event_revenue_idr"`
	EventSource 		string `json:"event_source"`
	IsReceiptValidated		string `json:"is_receipt_validated"`
	AfCostValue          string `json:"af_cost_value"`
	AfCostModel          string `json:"af_cost_model"`
	AfCampaign           string `json:"af_campaign"`
	AfAdset              string `json:"af_adset"`
	AfAd                 string `json:"af_ad"`
	AfChannel            string `json:"af_channel"`
	AfCostCurrency       string `json:"af_cost_currency"`
	AfAdType             string `json:"af_ad_type"`
	AfReengagementWindow string `json:"af_reengagement_window"`
	AfSiteid             string `json:"af_siteid"`
	HttpReferrer         string `json:"http_referrer"`
	MediaSource          string `json:"media_source"`
	Campaign             string `json:"campaign"`
}
